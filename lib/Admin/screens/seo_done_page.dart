import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:seooptimization/Admin/widgets/text_animation.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:seooptimization/Admin/controllers/http_controllers.dart';
import 'package:seooptimization/Admin/widgets/circular_progress.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../snack_bar.dart';
import 'home_page.dart';

class SeoDoneScreen extends StatelessWidget {
  final String websiteName;
  final String id;
  final String websiteUrl;
  final String requestId;
  SeoDoneScreen({Key key, @required this.websiteName, @required this.id, @required this.websiteUrl,  @required this.requestId}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return _SeoDoneScreen(websiteName:websiteName,id: id,websiteUrl: websiteUrl,requestId: requestId,);
  }
}

class _SeoDoneScreen extends StatefulWidget {
  final String websiteName;
  final String id;
  final String websiteUrl;
  final String requestId;
  _SeoDoneScreen(
      {Key key,
        @required this.websiteName, @required this.id, @required this.websiteUrl, @required this.requestId
      })
      : super(key: key);
  @override
  _SeoDoneScreenState createState() => _SeoDoneScreenState();
}

class _SeoDoneScreenState extends State<_SeoDoneScreen> {
  String selectedItem;
  double _registerBtnSize = 315;
  double _btnBorder = 4;
  bool _isLoading = false;
  TextEditingController commentController = TextEditingController();
  int delayAmount = 1700;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextStyle style =
  TextStyle(fontFamily: 'Montserrat', fontSize: 20.0, color: Colors.black);
  var webView = Container();
  ProgressDialog pr;
  final GlobalKey<ScaffoldState> mScaffoldState =
  new GlobalKey<ScaffoldState>();

  FocusNode _textFocus = new FocusNode();
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    getProgressBarSetUp();
  }

  @override
  Widget build(BuildContext context) {
    final textCard = Container(
      child: Center(
          child: ShowUp(
            child: Text("SEO Done",style: TextStyle(color:Colors.black,fontSize: 25,fontWeight: FontWeight.w700)),
            delay: delayAmount,
          )),
    );
    final descrriptionCard = Card(
      elevation: 3,
      color: hexToColor("#f7fffb"),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      borderOnForeground: true,
      margin: EdgeInsets.only(right: 24, left: 24),
      child: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 6, left: 8, right: 6),
              child: TextField(
                keyboardType: TextInputType.multiline,
                textAlign: TextAlign.left,
                controller: commentController,
                maxLines: 10,
                decoration: new InputDecoration(
                    icon: Icon(
                      Icons.description,
                      size: 40,
                      color: hexToColor("#00a859"),
                    ),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding:
                    EdgeInsets.only(right: 8, bottom: 4, top: 10),
                    hintText: "Any Guidance"),
              ))
        ],
      ),
    );
    final headerbar = Container(
        height: 80,
        color: hexToColor("#f7fffb"),
        margin: EdgeInsets.only(top: 28.0),
        child: Card(
          color: hexToColor("#f7fffb"),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          elevation: 8.0,
          child: Stack(
            children: <Widget>[
              //padding: EdgeInsets.only(top:20.0, right: 8.0),

              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(top: 20.0, left: 8.0),
                  child: Row(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: hexToColor("#00a859"),
                        ),
                      ),
                      SizedBox(
                        width: 1,
                      ),
                      Text(
                        "SEO Done",
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
    return Scaffold(
      key: mScaffoldState,
      backgroundColor: hexToColor("#f7fffb"),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            headerbar,
            SizedBox(
              height: 30,
            ),
            textCard,
            SizedBox(
              height: 30,
            ),
            descrriptionCard,
            SizedBox(
              height: 50,
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              child: GestureDetector(
                onTap: () {
                  _sendNotificationTap();
                  _deleteSeoRequest();
                  _seoDoneTap();
                },
                child: Container(
                  margin: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                  width: MediaQuery.of(context).size.width,

                  // color: hexToColor("#00a859"),
                  height: 50,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: hexToColor("#00a859"),
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                  ),
                  child: _isLoading
                      ? CircularProgress()
                      : Text(
                    "SEO DONE",
                    textAlign: TextAlign.center,
                    style: style.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
          ],
        ),
      ),
    );
  }

  _seoDoneTap() {
    if (commentController.text.isNotEmpty) {
      setState(() {
        _isLoading = true;
        _registerBtnSize = 80;
        _btnBorder = 20;
      });
      HttpControllers()
          .sendSeoDone(
          name: widget.websiteName,
          websiteUrl: widget.websiteUrl,
          guidance: commentController.text,
          id: widget.id)
          .then((response) {
        setState(() {
          _isLoading = false;
          _registerBtnSize = 315;
          _btnBorder = 4;
          print("all $response");
        });
        var message;
        var status;
        String msg = 'Unpredicted Error';
        for (int i = 0; i < response.length; i++) {
          status = response[i]["response"];
          message = response[i]["msg"];
        }
        if (status == '200') {

          commentController.text = '';
          msg = "Seo has been done";
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AdminHomePage(),
            ),
          );
        } else if (response == '22') {
          // Database Error
          msg = 'Server is not responding!';
        } else if (response == '33') {
          // Unpredicted Error
          msg = 'Unpredicted Error';
        } else {
          // Random Error
          msg = 'Unpredicted Error';
        }
        Sbar().showSnackBar(msg, mScaffoldState);
      });
    } else {
      Sbar().showSnackBar('Fill Required Fields', mScaffoldState);
    }
  }
  _sendNotificationTap() {

    HttpControllers()
        .sendNotification(
        user: widget.id,
        title: "SEO Done",
        body: "Seo has done for your ${widget.websiteName} website"
    )
        .then((response) {

      var message;
      var status;
      String msg = 'Unpredicted Error';
      for (int i = 0; i < response.length; i++) {
        status = response[i]["response"];
        message = response[i]["msg"];
      }

    });

  }
  _deleteSeoRequest() {

    HttpControllers()
        .removeRequest(int.parse(widget.requestId))
        .then((response) {

      var message;
      var status;
      String msg = 'Unpredicted Error';
        //
        // status = response["response"];
        // message = response["msg"];
        //

    });

  }

  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      _image = File(pickedFile.path);

      // uploadImage(pickedFile.path.toString());
    });
  }

  void getProgressBarSetUp() {
    pr = ProgressDialog(
      context,
      isDismissible: false,
    );
    pr.style(
        message: ' Uploading...',
        borderRadius: 4.0,
        backgroundColor: Colors.white,
        progressWidget: Padding(
          padding: EdgeInsets.all(8.0),
          child: CircularProgressIndicator(),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
  }
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(width: 1.0, color: Colors.grey),
    borderRadius:
    BorderRadius.all(Radius.circular(5.0) //         <--- border radius here
    ),
  );
}
