import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:seooptimization/Admin/controllers/http_controllers.dart';
import 'package:seooptimization/Admin/widgets/circular_progress.dart';
import 'package:seooptimization/widgets/timeago.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:seooptimization/Admin/screens/message_screen.dart';
import '../snack_bar.dart';



class AllMessagesScreen extends StatelessWidget {
  final String text;
  AllMessagesScreen({Key key, @required this.text}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return _AllMessagesScreen();
  }
}

class _AllMessagesScreen extends StatefulWidget {
  _AllMessagesScreen({Key key}) : super(key: key);
  @override
  _AllMessagesScreenState createState() => _AllMessagesScreenState();
}

class _AllMessagesScreenState extends State<_AllMessagesScreen> {
  bool isLoading = false;
  final GlobalKey<ScaffoldState> mScaffoldState =
  new GlobalKey<ScaffoldState>();
  var name = [];
  var id = [];
  var message = [];
  var messageTime = [];
  var distinctIds;
  @override
  void initState() {
    // TODO: implement initState
    _getAllMessages();
    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: mScaffoldState,
      backgroundColor: hexToColor("#f7fffb"),
      body:isLoading
          ? Center(
        child: CircularProgress(),
      )
          : message.isEmpty?
      Center(
        child: Text("No data"),
      ): SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 30,
            ),

            buildGetMessages(),
          ],
        ),
      ),
    );
  }
  Widget buildGetMessages() {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: BoxDecoration(
//                  color: Colors.lightGreen[100],
      ),
      child: ListView.builder(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          itemCount: name.length,
          itemBuilder: (BuildContext context, index) {
            return InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SingleMessageScreen(
                   id: distinctIds[index].toString(),
                        message: message[index],
                        messageTime: messageTime[index],
                        name: name[index],
                      ),
                    ),
                  );
                },
                child: Card(
                    elevation: 3,
                    child: ListTile(
                      leading:Icon(Icons.account_circle_rounded,size: 25,),
                      title:Text(name[index].toString(),  style: TextStyle(fontSize: 17, color: Colors.black54),),
                     trailing:  Text(
                       TimeAgo.timeAgoSinceDate(messageTime[index]),
                       style: TextStyle(fontSize: 14, color: Colors.black54),
                     ),
                    )));
          }),
    );}
  Future _getAllMessages() async {
    setState(() => isLoading = true);
    await HttpControllers().getUserMessages().then((response) {

      if (response != 'zero') {
        var extractedData = json.decode(response);
        print("data 1 $extractedData");
        for (int i = 0; i < extractedData.length; i++) {

          id.add(extractedData[i]["user"]);

        }
        distinctIds = id.toSet().toList();
        print("unique id $id");
        for (int i = 0; i < distinctIds.length; i++) {
          // if (distinctIds[i] == id[i]) {
            name.add(extractedData[i]["name"]);
            // id.add(extractedData[i]["user"]);
            message.add(extractedData[i]["message"]);
            messageTime.add(extractedData[i]["message_time"]);
          // }
        }
      } else {
        Sbar().showSnackBar('Access denied', mScaffoldState);
      }
      setState(() => isLoading = false);
    });
  }
  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(width: 1.0, color: Colors.grey),
    borderRadius:
    BorderRadius.all(Radius.circular(5.0) //         <--- border radius here
    ),
  );
}
