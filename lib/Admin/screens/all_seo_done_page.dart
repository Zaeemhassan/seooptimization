import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:seooptimization/Admin/controllers/http_controllers.dart';
import 'package:seooptimization/Admin/screens/progress_page.dart';
import 'package:seooptimization/Admin/widgets/circular_progress.dart';
import 'package:seooptimization/Admin/widgets/text_animation.dart';
import 'package:seooptimization/widgets/timeago.dart';
import 'package:seooptimization/Admin/screens/message_screen.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:seooptimization/Admin/screens/seo_done_page.dart';
import '../../snack_bar.dart';

class AllSeoDoneScreen extends StatelessWidget {
  final String text;
  AllSeoDoneScreen({Key key, @required this.text}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return _AllSeoDoneScreen();
  }
}

class _AllSeoDoneScreen extends StatefulWidget {
  _AllSeoDoneScreen({Key key}) : super(key: key);
  @override
  _AllSeoDoneScreenState createState() => _AllSeoDoneScreenState();
}

class _AllSeoDoneScreenState extends State<_AllSeoDoneScreen> {
  bool isLoading = false;
  final GlobalKey<ScaffoldState> mScaffoldState =
  new GlobalKey<ScaffoldState>();
  var time = [];
  var userId = [];
  var websiteName = [];
  var websiteUrl = [];
  var comment = [];
  int delayAmount = 1700;
  @override
  void initState() {
    // TODO: implement initState
    _getAllRequest();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final textCard = Container(
      child: Center(
          child: ShowUp(
            child: Text("All SEO Done",style: TextStyle(color:Colors.black,fontSize: 25,fontWeight: FontWeight.w700)),
            delay: delayAmount,
          )),
    );
    return Scaffold(
      key: mScaffoldState,
      backgroundColor: hexToColor("#f7fffb"),
      body: isLoading
          ? Center(
        child: CircularProgress(),
      )
          : websiteName.isEmpty?
      Center(
        child: Text(
          "No data"
        ),
      ):
      SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 20,
            ),
            textCard,
            buildGetSeoRequest(),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildGetSeoRequest() {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: BoxDecoration(
//                  color: Colors.lightGreen[100],
      ),
      child: ListView.builder(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          itemCount: websiteName.length,
          itemBuilder: (BuildContext context, index) {
            return InkWell(

                child: Card(
                    elevation: 3,
                    child: Container(
                      margin: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                      child: ClipPath(
                          child: Card(
                              shape: Border(
                                  left: BorderSide(
                                      color: Colors.green, width: 5)),
                              child: ExpansionTile(
                                leading: Container(
                                  margin: EdgeInsets.all(2),
                                  padding: EdgeInsets.all(2),
                                  decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(100),
                                      border: Border.all(
                                          width: 1, color: Colors.white)),
                                  child: Icon(
                                    Icons.check,
                                    color: Colors.white,
                                  ),
                                ),
                                title:Row(
                                 children:[
                                   Expanded(
                                child:Text(
                                  "${websiteName[index]}",
                                  style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w500),
                                )
                                   ),
                                   Expanded(
                                     child: Text(
                                       TimeAgo.timeAgoSinceDate(time[index].toString()),
                                       style: TextStyle(fontSize: 14, color: Colors.black54),
                                     ),
                                   )
                                 ]),
                                children: <Widget>[
                                  ListTile(
                                      title:Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children:[
                                            Text(
                                              "Website Url : ",
                                              style: TextStyle(
                                              ),
                                            ),
                                            InkWell(
                                              onTap: () async {
                                                if (await canLaunch(websiteUrl[index]) != null)
                                                  await launch(websiteUrl[index]);
                                                else
                                                  // can't launch url, there is some error
                                                  throw "Could not launch ${websiteUrl[index]}";

                                              },
                                              child:
                                              Text(
                                                  "${websiteUrl[index]}",
                                                  style: TextStyle(decoration: TextDecoration.underline, color: Colors.blue)
                                              ),),
                                            SizedBox(
                                              height: 4,
                                            ),

                                            Text(
                                              "Guidance : ${comment[index]}",
                                              style: TextStyle(
                                              ),
                                            ),
                                            SizedBox(
                                              height: 12,
                                            ),

                                          ]
                                      ))
                                ],
                              ))),
                    )));
          }),
    );
  }

  Future _getAllRequest() async {
    setState(() => isLoading = true);
    await HttpControllers().getAllSeoDone().then((response) {
      if (response != 'zero') {
        var extractedData = json.decode(response);
        print("data 1 $extractedData");
        for (int i = 0; i < extractedData.length; i++) {
          websiteName.add(extractedData[i]["website_name"]);
          websiteUrl.add(extractedData[i]["website_url"]);
          userId.add(extractedData[i]["user"]);
          comment.add(extractedData[i]["guidance"]);
          time.add(extractedData[i]["time"]);
        }
      } else {
        Sbar().showSnackBar('Access denied', mScaffoldState);
      }
      setState(() => isLoading = false);
    });
  }

  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(width: 1.0, color: Colors.grey),
    borderRadius:
    BorderRadius.all(Radius.circular(5.0) //         <--- border radius here
    ),
  );
}
