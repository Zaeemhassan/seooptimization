import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:seooptimization/Admin/controllers/http_controllers.dart';
import 'package:seooptimization/Admin/screens/progress_page.dart';
import 'package:seooptimization/Admin/widgets/circular_progress.dart';
import 'package:seooptimization/Admin/widgets/text_animation.dart';
import 'package:seooptimization/widgets/timeago.dart';
import 'package:seooptimization/Admin/screens/message_screen.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:seooptimization/Admin/screens/seo_done_page.dart';
import '../../snack_bar.dart';

class SeoRequestScreen extends StatelessWidget {
  final String text;
  SeoRequestScreen({Key key, @required this.text}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return _SeoRequestScreen();
  }
}

class _SeoRequestScreen extends StatefulWidget {
  _SeoRequestScreen({Key key}) : super(key: key);
  @override
  _SeoRequestScreenState createState() => _SeoRequestScreenState();
}

class _SeoRequestScreenState extends State<_SeoRequestScreen> {
  bool isLoading = false;
  final GlobalKey<ScaffoldState> mScaffoldState =
      new GlobalKey<ScaffoldState>();
  var keyword = [];
  var time = [];
  var userId = [];
  var websiteName = [];
  var websiteUrl = [];
  var loginName = [];
  var loginPassword = [];
  var deadline = [];
  var comment = [];
  var requestId = [];
  var pageUrl = [];
  var hostName = [];
  var hostPassword = [];
  var type = [];
  int delayAmount = 1700;
  @override
  void initState() {
    // TODO: implement initState
    _getAllRequest();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final textCard = Container(
      child: Center(
          child: ShowUp(
            child: Text("SEO Request",style: TextStyle(color:Colors.black,fontSize: 25,fontWeight: FontWeight.w700)),
            delay: delayAmount,
          )),
    );
    return Scaffold(
      key: mScaffoldState,
      backgroundColor: hexToColor("#f7fffb"),
      body: isLoading
          ? Center(
              child: CircularProgress(),
            )
          :websiteName.isEmpty?
      Center(
        child: Text("No data"),
      ):SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  textCard,
                  SizedBox(
                    height: 20,
                  ),
                  buildGetSeoRequest(),
                  SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
    );
  }

  Widget buildGetSeoRequest() {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: BoxDecoration(
//                  color: Colors.lightGreen[100],
          ),
      child: ListView.builder(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          itemCount: websiteName.length,
          itemBuilder: (BuildContext context, index) {
            return InkWell(

                child: Card(
                    elevation: 3,
                    child: Container(
                      margin: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                      child: ClipPath(
                          child: Card(
                              shape: Border(
                                  left: BorderSide(
                                      color: Colors.orange[500], width: 5)),
                              child: ExpansionTile(
                                leading: Container(
                                  margin: EdgeInsets.all(2),
                                  padding: EdgeInsets.all(2),
                                  decoration: BoxDecoration(
                                      color: Colors.orange,
                                      borderRadius: BorderRadius.circular(100),
                                      border: Border.all(
                                          width: 1, color: Colors.white)),
                                  child:  Icon(
                                    CupertinoIcons.info,
                                    color: Colors.white,
                                  ),
                                ),
                                title:Row(
                                children:[
                                  Expanded(
                                      child:Text(
                                  "${websiteName[index]}",
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w500),
                                )),
                                  Expanded(
                                    flex: -1,
                                    child: Text(
                                      TimeAgo.timeAgoSinceDate(time[index].toString()),
                                      style: TextStyle(fontSize: 14, color: Colors.black54),
                                    ),
                                  )
                                ]
                                ),
                                children: <Widget>[
                                  ListTile(
                                    title:Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                   children:[

                                     Text(
                                       "SEO Type : ${type[index] == null ?"": type[index].toString()}",
                                       style: TextStyle(
                                       ),
                                     ),
                                     SizedBox(
                                       height: 4,
                                     ),
                                     Text(
                                       "Website Url : ",
                                       style: TextStyle(
                                       ),
                                     ),
                                     InkWell(
                                       onTap: () async {
                                         if (await canLaunch(websiteUrl[index]) != null)
                                         await launch(websiteUrl[index]);
                                         else
                                         // can't launch url, there is some error
                                         throw "Could not launch ${websiteUrl[index]}";

                                       },
                                       child:
                                     Text(
                                      "${websiteUrl[index]}",
                                         style: TextStyle(decoration: TextDecoration.underline, color: Colors.blue)
                                    ),),
                                     SizedBox(
                                       height: 4,
                                     ),
                                     Text(
                                       "Login Username : ${loginName[index]}",
                                       style: TextStyle(
                                       ),
                                     ),
                                     SizedBox(
                                       height: 4,
                                     ),
                                     Text(
                                       "Login Password : ${loginPassword[index]}",
                                       style: TextStyle(
                                       ),
                                     ),
                                     SizedBox(
                                       height: 4,
                                     ),
                                     Text(
                                       "Keywords : ${keyword[index]}",
                                       style: TextStyle(
                                       ),
                                     ),
                                     SizedBox(
                                       height: 4,
                                     ),
                                     Text(
                                       "Hosting Username : ${hostName[index] == null? "":hostName[index].toString()}",
                                       style: TextStyle(
                                       ),
                                     ),
                                     SizedBox(
                                       height: 4,
                                     ),
                                     Text(
                                       "Hosting Password : ${hostPassword[index] == null? "":hostPassword[index].toString()}",
                                       style: TextStyle(
                                       ),
                                     ),
                                     SizedBox(
                                       height: 4,
                                     ),
                                     Text(
                                       "Page Url : ${pageUrl[index] == null? "":pageUrl[index].toString()}",
                                       style: TextStyle(
                                       ),
                                     ),
                                     SizedBox(
                                       height: 4,
                                     ),
                                     Text(
                                       "Deadline : ${deadline[index]}",
                                       style: TextStyle(
                                       ),
                                     ),
                                     SizedBox(
                                       height: 4,
                                     ),
                                     Text(
                                       "Additional Comment : ${comment[index]}",
                                       style: TextStyle(
                                       ),
                                     ),
                                     SizedBox(
                                       height: 12,
                                     ),
                                     Container(
                                         height: 30,
                                         width: 140,
                                         decoration: BoxDecoration(
                                           borderRadius: BorderRadius.circular(20.0),
                                           border: Border.all(
                                               color: Colors.green
                                           ),
                                         ),
                                         child: InkWell(
                                             onTap: (){
                                               Navigator.push(
                                                 context,
                                                 MaterialPageRoute(
                                                   builder: (context) => ProgressScreen(id: userId[index].toString()),
                                                 ),
                                               );
                                             },
                                             child:Center(
                                                 child:Text(
                                                   "Send Progress",
                                                   style: TextStyle(
                                                       color: Colors.green
                                                   ),
                                                 )

                                             ))),
                                     SizedBox(
                                       height: 12,
                                     ),
                                     Container(
                                         height: 30,
                                         width: 140,
                                         decoration: BoxDecoration(
                                           borderRadius: BorderRadius.circular(20.0),
                                           border: Border.all(
                                               color: Colors.green
                                           ),
                                         ),
                                         child: InkWell(
                                             onTap: (){
                                               Navigator.push(
                                                 context,
                                                 MaterialPageRoute(
                                                   builder: (context) => SeoDoneScreen(
                                                       websiteName: websiteName[index],
                                                       id: userId[index].toString(),
                                                       websiteUrl: websiteUrl[index],
                                                     requestId: requestId[index].toString(),

                                                       ),
                                                 ),
                                               );
                                             },
                                             child:Center(
                                                 child:Text(
                                                   "Seo Done",
                                                   style: TextStyle(
                                                       color: Colors.green
                                                   ),
                                                 )

                                             ))),
                                     SizedBox(
                                       height: 10,
                                     )
                                    ]
                                  ))
                                ],
                              ))),
                    )));
          }),
    );
  }

  Future _getAllRequest() async {
    setState(() => isLoading = true);
    await HttpControllers().getSeoRequest().then((response) {
      if (response != 'zero') {
        var extractedData = json.decode(response);
        print("data 1 $extractedData");
        for (int i = 0; i < extractedData.length; i++) {
          keyword.add(extractedData[i]["keywords"]);
          // id.add(extractedData[i]["user"]);
          websiteName.add(extractedData[i]["website_name"]);
          websiteUrl.add(extractedData[i]["website_url"]);
          time.add(extractedData[i]["time"]);
          userId.add(extractedData[i]["user"]);
          loginName.add(extractedData[i]["login_username"]);
          loginPassword.add(extractedData[i]["login_password"]);
          deadline.add(extractedData[i]["deadline"]);
          comment.add(extractedData[i]["additional_comment"]);
          requestId.add(extractedData[i]["request_id"]);
          type.add(extractedData[i]["type"]);
          pageUrl.add(extractedData[i]["url"]);
          hostPassword.add(extractedData[i]["host_password"]);
          hostName.add(extractedData[i]["host_name"]);
        }
        print("done id $userId");
      } else {
        Sbar().showSnackBar('Access denied', mScaffoldState);
      }
      setState(() => isLoading = false);
    });
  }

  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(width: 1.0, color: Colors.grey),
    borderRadius:
        BorderRadius.all(Radius.circular(5.0) //         <--- border radius here
            ),
  );
}
