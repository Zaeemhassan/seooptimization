import 'dart:io';
import 'package:intl/intl.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:seooptimization/Admin/controllers/http_controllers.dart';
import 'dart:async';
import 'dart:convert';
import 'package:seooptimization/widgets/timeago.dart';
import 'package:seooptimization/widgets/circular_progress.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../snack_bar.dart';

class SingleMessageScreen extends StatefulWidget {
  final String id;
  final String message;
  final String messageTime;
  final String name;
  SingleMessageScreen(
      {Key key,
        @required this.id,
        @required this.message,
        @required this.messageTime,
        @required this.name,
      })
      : super(key: key);

  @override
  _SingleMessageScreenState createState() => _SingleMessageScreenState();
}

class _SingleMessageScreenState extends State<SingleMessageScreen> {

  final GlobalKey<ScaffoldState> mScaffoldState =
  new GlobalKey<ScaffoldState>();
  var id = [];
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();
  List document = [];
  bool isLoading = false;
  var currentDateTime;
  String formattedDate;
  var fName;
  var lName;
  var userId;
  var name = [];
  var message = [];
  var dateTime = [];
  TextEditingController commentController = TextEditingController();
  @override
  void initState() {
    super.initState();
    print("disid ${widget.id}");
    _getMessages();
    _getAllMessages();


  }

  @override
  Widget build(BuildContext context) {
    final headerbar = Container(
        height: 80,
        color: hexToColor("#f7fffb"),
        margin: EdgeInsets.only(top: 28.0),
        child: Card(
          color: hexToColor("#f7fffb"),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          elevation: 8.0,
          child: Stack(
            children: <Widget>[
              //padding: EdgeInsets.only(top:20.0, right: 8.0),

              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(top: 20.0, left: 8.0),
                  child: Row(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: hexToColor("#00a859"),
                        ),
                      ),
                      SizedBox(
                        width: 1,
                      ),
                      Text(
                        "User Message",
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
    return Scaffold(
        key: mScaffoldState,
        body:RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () async {
              _getMessages();
               _getAllMessages();


            },
            child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 5,
                  ),
                  headerbar,
                  Expanded(child:
                  ListView(
                    children:[
                      isLoading
                          ? Center(
                        child: CircularProgress(),
                      )
                          :
                           buildGetAdminReply(),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                  ),
                  Expanded(
                      flex: -3,
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                            margin: const EdgeInsets.all(10),
                            child: Row(
                              // textDirection: TextDirection.RTL
                              children: <Widget>[
                                Expanded(
                                    child: TextFormField(
                                      controller: commentController,
                                      decoration: new InputDecoration(
                                          hintText: "Enter message",
                                          hintStyle: TextStyle(color: Colors.black)),
                                    )),
                                IconButton(icon: Icon(Icons.send), onPressed: () {
                                  DateTime now = new DateTime.now();
                                  formattedDate = DateFormat('dd-MM-yyyy h:mma').format(now);
                                  currentDateTime = formattedDate;
                                  if(commentController.text.isEmpty){
                                    Sbar().showSnackBar(
                                        "Please enter your comment", mScaffoldState);
                                  }
                                  else{

                                    _addMessage();
                                    _getMessages();
                                    _getAllMessages();
                                    commentController.text = "";
                                  }
                                }),

                              ],
                            )),
                      ))
                ]))
    );
  }
  Widget buildGetAdminReply() {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: BoxDecoration(
//                  color: Colors.lightGreen[100],
      ),
      child: ListView.builder(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          itemCount: name.length,
          itemBuilder: (BuildContext context, index) {
            return InkWell(
                onTap: () {},
                child: Card(
                    elevation: 3,
                    child: ListTile(
                      leading:Icon(Icons.account_circle_rounded,size: 25,),
                      title:Row(
                          children:[
                            Expanded(
                              child:
                              Text(
                                name[index],
                                style: TextStyle(fontSize: 17, color: Colors.black,fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(width: 15,),
                            Expanded(
                                child:
                                Text(
                                  TimeAgo.timeAgoSinceDate(dateTime[index]),
                                  style: TextStyle(fontSize: 14, color: Colors.black54),
                                )
                            )
                          ]
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Divider(),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            message[index],
                            style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'EmojiOne',
                            ),
                          )
                        ],
                      ),
                    )));
          }),
    );
  }

  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }
  Future _getMessages() async {
    name.clear();
    message.clear();
    dateTime.clear();
    id.clear();
    setState(() => isLoading = true);
    await getUserPreference();
    await HttpControllers().getSingleMessages((widget.id)).then((response) {

      if (response != 'zero') {
        var extractedData = json.decode(response);
        print("data 1 $extractedData");
        for(int i = 0; i< extractedData.length; i++) {
          name.add(extractedData[i]["name"]);
          message.add(extractedData[i]["message"]);
          dateTime.add(extractedData[i]["time"]);
        }
      } else {
        debugPrint("Access Denied");
      }
    });
    setState(() => isLoading = false);
  }
  Future _getAllMessages() async {
    id.clear();
    setState(() => isLoading = true);
    await HttpControllers().getUserMessages().then((response) {

      if (response != 'zero') {
        var extractedData = json.decode(response);

        for (int i = 0; i < extractedData.length; i++) {
          id.add(extractedData[i]["user"]);
        }

      print("idddddd $id");
        for (int i = 0; i < extractedData.length; i++) {
          if(int.parse(widget.id) == id[i]) {
            name.add(extractedData[i]["name"]);
            // id.add(extractedData[i]["user"]);
            message.add(extractedData[i]["message"]);
            dateTime.add(extractedData[i]["message_time"]);
          }
        }
        print("name $name");
      } else {
        debugPrint("Access Denied");
      }
      setState(() => isLoading = false);
    });
  }
  _addMessage() {
    if (commentController.text.isNotEmpty) {
      setState(() {
        isLoading = true;
      });
      HttpControllers()
          .sendMessage(
          name: "Admin",
          id: widget.id,
          message: commentController.text)
          .then((response) {
        setState(() {
          isLoading = false;
          print("all $response");
        });
        var message;
        var status;
        String msg = 'Unpredicted Error';
        for (int i = 0; i < response.length; i++) {
          status = response[i]["response"];
          message = response[i]["msg"];
        }
        if (status == '200') {
          msg = "Your message has been transferred";
        } else if (response == '22') {
          // Database Error
          msg = 'Server is not responding!';
        } else if (response == '33') {
          // Unpredicted Error
          msg = 'Unpredicted Error';
        } else {
          // Random Error
          msg = 'Unpredicted Error';
        }
        Sbar().showSnackBar(msg, mScaffoldState);
      });
    } else {
      Sbar().showSnackBar('Fill Required Fields', mScaffoldState);
    }
  }
  Future getUserPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    fName = prefs.getString("firstName");
    lName = prefs.getString("lastName");
    userId = prefs.getString("UserId");

  }
}