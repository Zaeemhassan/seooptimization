import 'dart:async';
import 'dart:convert';
import 'package:seooptimization/Admin/model/notification.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:seooptimization/Admin/controllers/http_controllers.dart';
import 'package:seooptimization/Admin/widgets/circular_progress.dart';
import 'package:seooptimization/widgets/timeago.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:seooptimization/Admin/screens/message_screen.dart';
import '../snack_bar.dart';
import 'home_page.dart';



class NotificationScreen extends StatelessWidget {
  final String text;
  NotificationScreen({Key key, @required this.text}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return _NotificationScreen();
  }
}

class _NotificationScreen extends StatefulWidget {
  _NotificationScreen({Key key}) : super(key: key);
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<_NotificationScreen> {
  bool isLoading = false;
  final GlobalKey<ScaffoldState> mScaffoldState =
  new GlobalKey<ScaffoldState>();
  var title = [];
  var id = [];
  var body = [];
  var messageTime = [];
  bool isSeen = false;
  var distinctIds;
  var _notification = new List<UserNotification>();
  var notifyId;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();
  @override
  void initState() {
    // TODO: implement initState
    _getAllMessages();
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    final headerbar = Container(
        height: 80,
        color: hexToColor("#f7fffb"),
        margin: EdgeInsets.only(top: 28.0),
        child: Card(
          color: hexToColor("#f7fffb"),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          elevation: 8.0,
          child: Stack(
            children: <Widget>[
              //padding: EdgeInsets.only(top:20.0, right: 8.0),

              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(top: 20.0, left: 8.0),
                  child: Row(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: hexToColor("#00a859"),
                        ),
                      ),
                      SizedBox(
                        width: 1,
                      ),
                      Text(
                        "Notification",
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
    return Scaffold(
      key: mScaffoldState,
      backgroundColor: hexToColor("#f7fffb"),
      body: RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () async => await _getAllMessages(),
    child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            headerbar,
            SizedBox(
              height: 10,
            ),
            isLoading
                ? Center(
              child: CircularProgress(),
            )
                :
            buildGetMessages(),
          ],
        ),
      ),)
    );
  }
  Widget buildGetMessages() {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: BoxDecoration(
//                  color: Colors.lightGreen[100],
      ),
      child: ListView.builder(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          itemCount: _notification.length,
          itemBuilder: (BuildContext context, index) {
            return InkWell(
                onTap: (){
                  print("hello");
                  notifyId = _notification[index].notifyId;
                  _updateNotification();
                },
                child:Card(
                    color:  _notification[index].seen == false ? Colors.white38 : Colors.white,
                    elevation: 3,
                    child: ListTile(
                      leading: CircleAvatar(

                        foregroundColor: Colors.white,
                        backgroundColor: Colors.green,
                        child: Icon(CupertinoIcons.bell_circle_fill),
                      ),
                      title: Text(
                        _notification[index].title,
                        style:
                        TextStyle(fontSize: 15, color: Colors.black,fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(
                        _notification[index].body,
                        style:
                        TextStyle(fontSize: 15, color: Colors.black),
                      ),
                      trailing:  Text(
                        TimeAgo.timeAgoSinceDate(_notification[index].dateTime.toString()),
                        style:
                        TextStyle(fontSize: 12, color: Colors.black,

                        ),),

                    )));

          }),
    );}
  Future _getAllMessages() async {
    setState(() => isLoading = true);
    await HttpControllers().getNotification().then((response) {

      if (response != 'zero') {
        var extractedData = json.decode(response);
        print("data 1 $extractedData");

        for (int i = 0; i < extractedData.length; i++) {
          final UserNotification a = UserNotification(
            extractedData[i]["title"],
            extractedData[i]["body"],
            extractedData[i]["date_time"],
            extractedData[i]["user"],
            extractedData[i]["notification_id"],
            extractedData[i]["status"],
          );

          _notification.add(a);
        }
      } else {
        Sbar().showSnackBar('Access denied', mScaffoldState);
      }
      setState(() => isLoading = false);
    });
  }
  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }
  _updateNotification() {

    HttpControllers()
        .updateNotification(notifyId)
        .then((response) {

      var message;
      var status;
      String msg = 'Unpredicted Error';
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AdminHomePage(),
        ),
      );
      //
      // status = response["response"];
      // message = response["msg"];
      //

    });

  }
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(width: 1.0, color: Colors.grey),
    borderRadius:
    BorderRadius.all(Radius.circular(5.0) //         <--- border radius here
    ),
  );
}
