import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HttpControllers {
  //* * * * * * *   LOGIN   * * * * * * *
  Future login({String email, String password}) async {
    try {
      final json = jsonEncode({
        "email": "$email",
        "password": "$password",
      });
      var url = Uri.parse('http://139.59.80.110:8187/seo/auth');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }

  //* * * * * * *   Registration * * * * * * *//
  Future registerUser({
    String firstName,
    String lastName,
    String email,
    String location,
    String userName,
    String password,
  }) async {
    try {
      print("data $firstName");
      final json = jsonEncode({
        "first_name": "$firstName",
        "last_name": "$lastName",
        "address": "$location",
        "email": "$email",
        "password": "$password"
      });
      var url = Uri.parse('http://139.59.80.110:8187/seo/admin/register');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});

      print("staty ${response.statusCode}");
      debugPrint("Response: " + response.body);
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];

        return response.body;
      } else if (response.statusCode == 409) {
        return "409";
      } else {
        // 33 is the ERR CODE
        return '22';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '22';
    }
  }

  //* * * * * * *  Update Password  * * * * * * *//
  Future updatePassword(
      {String oldPassword, String newPassword, String id}) async {
    try {
      final json = jsonEncode({
        "user_id": "$id",
        "old_password": "$oldPassword",
        "new_password": "$newPassword",
      });
      var url = Uri.parse('http://139.59.80.110:8187/seo/password/change');
      var response = await http
          .put(url, body: json, headers: {"Content-Type": "application/json"});

      print(response.statusCode);
      debugPrint("Response: " + response.body);
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else if (response.statusCode == 409) {
        return "409";
      } else {
        // 33 is the ERR CODE
        return '22';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '22';
    }
  }

  //* * * * * * *   Add Tips   * * * * * * *
  Future addTips({String title, String description}) async {
    try {
      final json = jsonEncode({
        "title": "$title",
        "description": "$description",
      });
      var url = Uri.parse('http://139.59.80.110:8187/seo/tips');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }

  //* * * * * * *   Update Tips   * * * * * * *
  Future updateTips({String id, String title, String description}) async {
    try {
      final json = jsonEncode({
        "tips_id": "$id",
        "title": "$title",
        "description": "$description",
      });
      var url = Uri.parse('http://139.59.80.110:8187/seo/tips/update');
      var response = await http
          .put(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }

  //* * * * * * *   UpdateProfile   * * * * * * *
  Future updateProfile(
      {String fName,
      String lName,
      String email,
      String address,
      String id}) async {
    try {
      final json = jsonEncode({
        "user_id": "$id",
        "first_name": "$fName",
        "last_name": "$lName",
        "email": "$email",
        "address": "$address"
      });
      var url = Uri.parse('http://139.59.80.110:8187/seo/admin/update');
      var response = await http
          .put(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }

  //* * * * * * *   Get Tips   * * * * * * *
  Future getTips({String title, String description}) async {
    try {
      var url = Uri.parse('http://139.59.80.110:8187/seo/tips');
      var response =
          await http.get(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("data ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }

  //* * * * * * *   Delete Tips   * * * * * * *
  Future deleteTips({String id}) async {
    try {
      var url =
          Uri.parse('http://139.59.80.110:8187/seo/delete/tip?tips_id=$id');
      var response =
          await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("data ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }

  //* * * * * * *  Get Profile   * * * * * * *
  Future getProfile({String id}) async {
    try {
      var url = Uri.parse('http://139.59.80.110:8187/seo/profile?user_id=$id');
      var response =
          await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Get User Messages   * * * * * * *
  Future getUserMessages() async {
    try {
      var url = Uri.parse('http://139.59.80.110:8188/seo/message');
      var response =
      await http.get(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("usermessage ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Send Message   * * * * * * *
  Future sendMessage({String name,String id, String message}) async {
    print("send id $id");
    try {
      final json = jsonEncode({
        "name": "$name",
        "reply_to" : id,
        "message": "$message",
      });
      var url = Uri.parse('http://139.59.80.110:8188/seo/reply');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Get Messages   * * * * * * *
  Future getSingleMessages(String id) async {
    print("reply id $id");
    try {
      var url = Uri.parse('http://139.59.80.110:8188/seo/get/reply?reply_to=$id');
      var response =
      await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("data ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Get User Seo Service   * * * * * * *
  Future getSeoRequest() async {
    try {
      var url = Uri.parse('http://139.59.80.110:8186/seo/service');
      var response =
      await http.get(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("usermessage ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Send Progress   * * * * * * *
  Future sendProgress({String percentage,String id}) async {
    print("send id $id");
    try {
      final json = jsonEncode({
        "percentage": "$percentage",
        "user" : "$id",
      });
      var url = Uri.parse('http://139.59.80.110:8186/seo/progress');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Send Message   * * * * * * *
  Future sendSeoDone({String name, String websiteUrl, String guidance, String id}) async {
    print("send id $id");
    try {
      final json = jsonEncode({
        "website_name": "$name",
        "website_url" : "$websiteUrl",
        "guidance": "$guidance",
        "user":"$id"
      });
      var url = Uri.parse('http://139.59.80.110:8186/seo/done');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Send Notification  * * * * * * *
  Future sendNotification({String user, String title, String body}) async {
print("notiid $user");
    try {
      final json = jsonEncode({
        "user": "$user",
        "title" : "$title",
        "body": "$body",
        "status":false
      });
      var url = Uri.parse('http://139.59.80.110:8186/seo/admin/notify');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Get Notification   * * * * * * *
  Future getNotification() async {
    try {
      var url = Uri.parse('http://139.59.80.110:8186/seo/user/view/notification');
      var response =
      await http.get(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("usermessage ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Delete SEO Request   * * * * * * *
  Future removeRequest(int requestId) async {
    print("request $requestId");
    try {
      var url = Uri.parse('http://139.59.80.110:8186/seo/delete/request?id=$requestId');
      var response =
      await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("usermessage ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Update Notification   * * * * * * *
  Future updateNotification(int notifyId) async {
    print("notifyId $notifyId");
    try {
      var url = Uri.parse('http://139.59.80.110:8186/seo/user/seen?user=$notifyId');
      var response =
      await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("usermessage ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   All Seo Done   * * * * * * *
  Future getAllSeoDone() async {
    try {
      var url = Uri.parse('http://139.59.80.110:8186/seo/show/done');
      var response =
      await http.get(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("usermessage ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
}
