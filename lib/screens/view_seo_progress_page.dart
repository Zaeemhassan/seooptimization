import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:seooptimization/controllers/http_controllers.dart';
import 'package:seooptimization/widgets/circular_progress.dart';
import 'package:seooptimization/widgets/timeago.dart';
import 'package:seooptimization/Admin/screens/message_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../snack_bar.dart';


class UserProgressScreen extends StatelessWidget {
  final String text;
  UserProgressScreen({Key key, @required this.text}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return _UserProgressScreen();
  }
}

class _UserProgressScreen extends StatefulWidget {
  _UserProgressScreen({Key key}) : super(key: key);
  @override
  _UserProgressScreenState createState() => _UserProgressScreenState();
}

class _UserProgressScreenState extends State<_UserProgressScreen> {
  bool isLoading = false;
  final GlobalKey<ScaffoldState> mScaffoldState =
  new GlobalKey<ScaffoldState>();
  var percentage = [];
  var time = [];
  var userId;

  @override
  void initState() {
    // TODO: implement initState

   _getAllProgress();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: mScaffoldState,
      backgroundColor: hexToColor("#f7fffb"),
      body: isLoading
          ? Center(
        child: CircularProgress(),
      )
          :percentage.isEmpty?
      Center(child:
        Text("No data"),):SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            buildGetSeoRequest(),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildGetSeoRequest() {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: BoxDecoration(
//                  color: Colors.lightGreen[100],
      ),
      child: ListView.builder(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          itemCount: percentage.length,
          itemBuilder: (BuildContext context, index) {
            return InkWell(

                child:  Container(
                      margin: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                      child: ClipPath(
                          child: Card(
                              shape: Border(
                                  left: BorderSide(
                                      color: Colors.green, width: 5)),
                              child: ExpansionTile(
                                leading: Container(
                                  margin: EdgeInsets.all(2),
                                  padding: EdgeInsets.all(2),
                                  decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(100),
                                      border: Border.all(
                                          width: 1, color: Colors.white)),
                                  child: Icon(
                                    Icons.check,
                                    color: Colors.white,
                                  ),
                                ),
                                title:Row(
                                  children:[
                                    Expanded(child:
                                Text(

                                  "${percentage[index]}%",
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w500),
                                ),),
                                    Expanded(
                                      child: Text(
                                        TimeAgo.timeAgoSinceDate(time[index].toString()),
                                        style: TextStyle(fontSize: 14, color: Colors.black54),
                                      ),
                                    )
                                ]
                                ),
                                children: <Widget>[
                                  ListTile(
                                      title:Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children:[
                                            Padding(
                                              padding: EdgeInsets.all(5.0),
                                              child: LinearPercentIndicator(
                                                width: MediaQuery.of(context).size.width - 100,
                                                animation: true,
                                                lineHeight: 20.0,
                                                animationDuration: 2000,
                                                percent: int.parse(percentage[index])/100,
                                                animateFromLastPercent: true,
                                                center: Text(
                                                  "${percentage[index].toString()}%",
                                                  style: TextStyle(color: Colors.white),
                                                ),
                                                linearStrokeCap: LinearStrokeCap.roundAll,
                                                progressColor: Colors.green[400],
                                                maskFilter: MaskFilter.blur(BlurStyle.solid, 3),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            )
                                          ]
                                      ))
                                ],
                              ))),
                    ));
          }),
    );
  }

  Future _getAllProgress() async {
    await getUserPreference();
    setState(() => isLoading = true);
    await HttpControllers().getSeoProgress(int.parse(userId)).then((response) {
      if (response != 'zero') {
        var extractedData = json.decode(response);
        print("data 1 $extractedData");
        for (int i = 0; i < extractedData.length; i++) {
          time.add(extractedData[i]["_time"]);
          percentage.add(extractedData[i]["percentage"]);
        }
      } else {
        Sbar().showSnackBar('Access denied', mScaffoldState);
      }
      setState(() => isLoading = false);
    });
  }
  Future getUserPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = prefs.getString("UserId");


  }
  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(width: 1.0, color: Colors.grey),
    borderRadius:
    BorderRadius.all(Radius.circular(5.0) //         <--- border radius here
    ),
  );
}
