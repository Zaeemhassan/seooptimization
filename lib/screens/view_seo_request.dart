import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:seooptimization/controllers/http_controllers.dart';
import 'package:seooptimization/widgets/circular_progress.dart';
import 'package:seooptimization/widgets/text_animation.dart';
import 'package:seooptimization/widgets/timeago.dart';
import 'package:seooptimization/Admin/screens/message_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:seooptimization/Admin/screens/seo_done_page.dart';
import '../snack_bar.dart';

class SeoRequestScreen extends StatelessWidget {
  final String text;
  SeoRequestScreen({Key key, @required this.text}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return _SeoRequestScreen();
  }
}

class _SeoRequestScreen extends StatefulWidget {
  _SeoRequestScreen({Key key}) : super(key: key);
  @override
  _SeoRequestScreenState createState() => _SeoRequestScreenState();
}

class _SeoRequestScreenState extends State<_SeoRequestScreen> {
  bool isLoading = false;
  final GlobalKey<ScaffoldState> mScaffoldState =
  new GlobalKey<ScaffoldState>();
  var keyword = [];
  var time = [];
  var userId = [];
  var websiteName = [];
  var websiteUrl = [];
  var loginName = [];
  var loginPassword = [];
  var deadline = [];
  var comment = [];
  var requestId = [];
  var id;
  int delayAmount = 1700;
  @override
  void initState() {
    // TODO: implement initState
    _getAllRequest();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final headerbar = Container(
        height: 80,
        color: hexToColor("#f7fffb"),
        margin: EdgeInsets.only(top: 28.0),
        child: Card(
          color: hexToColor("#f7fffb"),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          elevation: 8.0,
          child: Stack(
            children: <Widget>[
              //padding: EdgeInsets.only(top:20.0, right: 8.0),

              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(top: 20.0, left: 8.0),
                  child: Row(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: hexToColor("#00a859"),
                        ),
                      ),
                      SizedBox(
                        width: 1,
                      ),
                      Text(
                        "SEO Request",
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
    final textCard = Container(
      child: Center(
          child: ShowUp(
            child: Text("SEO Request",style: TextStyle(color:Colors.black,fontSize: 25,fontWeight: FontWeight.w700)),
            delay: delayAmount,
          )),
    );
    return Scaffold(
      key: mScaffoldState,
      backgroundColor: hexToColor("#f7fffb"),
      body: isLoading
          ? Center(
        child: CircularProgress(),
      )
          : SingleChildScrollView(
        child: Column(
          children: <Widget>[
            headerbar,

            SizedBox(
              height: 20,
            ),
            textCard,
            SizedBox(
              height: 20,
            ),
            websiteName.isEmpty?
            Center(
                child:Text("No data",style: TextStyle(fontSize: 16),)):
            buildGetSeoRequest(),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildGetSeoRequest() {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: BoxDecoration(
//                  color: Colors.lightGreen[100],
      ),
      child: ListView.builder(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          itemCount: websiteName.length,
          itemBuilder: (BuildContext context, index) {
            return InkWell(

                child: Card(
                    elevation: 3,
                    child: Container(
                      margin: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                      child: ClipPath(
                          child: Card(
                              shape: Border(
                                  left: BorderSide(
                                      color: Colors.orange[500], width: 5)),
                              child: ExpansionTile(
                                leading: Container(
                                  margin: EdgeInsets.all(2),
                                  padding: EdgeInsets.all(2),
                                  decoration: BoxDecoration(
                                      color: Colors.orange,
                                      borderRadius: BorderRadius.circular(100),
                                      border: Border.all(
                                          width: 1, color: Colors.white)),
                                  child:  Icon(
                                    CupertinoIcons.info,
                                    color: Colors.white,
                                  ),
                                ),
                                title:Row(
                                children:[
                                  Expanded(
                                 child:Text(
                                  "${websiteName[index]}",
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w500),
                                ),),
                                  Expanded(
                                    flex: -1,
                                    child: Text(
                                      TimeAgo.timeAgoSinceDate(time[index].toString()),
                                      style: TextStyle(fontSize: 14, color: Colors.black54),
                                    ),
                                  )
                                ]
                                ),
                                children: <Widget>[
                                  ListTile(
                                      title:Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children:[
                                            Text(
                                              "Website Url : ",
                                              style: TextStyle(
                                              ),
                                            ),
                                            InkWell(
                                              onTap: () async {
                                                if (await canLaunch(websiteUrl[index]) != null)
                                                  await launch(websiteUrl[index]);
                                                else
                                                  // can't launch url, there is some error
                                                  throw "Could not launch ${websiteUrl[index]}";

                                              },
                                              child:
                                              Text(
                                                  "${websiteUrl[index]}",
                                                  style: TextStyle(decoration: TextDecoration.underline, color: Colors.blue)
                                              ),),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text(
                                              "Login Username : ${loginName[index]}",
                                              style: TextStyle(
                                              ),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text(
                                              "Login Password : ${loginPassword[index]}",
                                              style: TextStyle(
                                              ),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text(
                                              "Keywords : ${keyword[index]}",
                                              style: TextStyle(
                                              ),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text(
                                              "Deadline : ${deadline[index]}",
                                              style: TextStyle(
                                              ),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text(
                                              "Additional Comment : ${comment[index]}",
                                              style: TextStyle(
                                              ),
                                            ),
                                            SizedBox(
                                              height: 12,
                                            ),

                                          ]
                                      ))
                                ],
                              ))),
                    )));
          }),
    );
  }

  Future _getAllRequest() async {
    await getUserPreference();
    setState(() => isLoading = true);
    await HttpControllers().getSeoRequest(int.parse(id)).then((response) {
      if (response != 'zero') {
        var extractedData = json.decode(response);
        print("data 1 $extractedData");
        for (int i = 0; i < extractedData.length; i++) {
          keyword.add(extractedData[i]["keywords"]);
          // id.add(extractedData[i]["user"]);
          websiteName.add(extractedData[i]["website_name"]);
          websiteUrl.add(extractedData[i]["website_url"]);
          time.add(extractedData[i]["_time"]);
          userId.add(extractedData[i]["user"]);
          loginName.add(extractedData[i]["login_username"]);
          loginPassword.add(extractedData[i]["login_password"]);
          deadline.add(extractedData[i]["deadline"]);
          comment.add(extractedData[i]["additional_comment"]);
          requestId.add(extractedData[i]["request_id"]);
        }
      } else {
        Sbar().showSnackBar('Access denied', mScaffoldState);
      }
      setState(() => isLoading = false);
    });
  }

  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }
  Future getUserPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    id = prefs.getString("UserId");


  }
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(width: 1.0, color: Colors.grey),
    borderRadius:
    BorderRadius.all(Radius.circular(5.0) //         <--- border radius here
    ),
  );
}
