import 'dart:async';
import 'package:seooptimization/widgets/text_animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:seooptimization/controllers/http_controllers.dart';
import 'package:seooptimization/widgets/circular_progress.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:seooptimization/screens/payment_page.dart';
import '../snack_bar.dart';

class RequestSEOScreen extends StatelessWidget {
  final String text;
  RequestSEOScreen({Key key, @required this.text}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return _RequestSEOScreen();
  }
}

class _RequestSEOScreen extends StatefulWidget {
  _RequestSEOScreen({Key key}) : super(key: key);
  @override
  _RequestSEOScreenState createState() => _RequestSEOScreenState();
}

class _RequestSEOScreenState extends State<_RequestSEOScreen> {
  int delayAmount = 1700;
  String selectedItem;
  double _registerBtnSize = 315;
  double _btnBorder = 4;
  bool _isLoading = false;
  var fName;
  TextEditingController nameController = TextEditingController();
  TextEditingController urlController = TextEditingController();
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController keywordController = TextEditingController();
  TextEditingController commentController = TextEditingController();
  TextEditingController pageUrlController = TextEditingController();
  TextEditingController hostNameController = TextEditingController();
  TextEditingController hostPasswordController = TextEditingController();
  var userId;
  var selected, selectedType;
  List category = ["On page seo","Off page seo","Technical seo"];
  String deadlineTime = "Select Deadline";
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextStyle style =
  TextStyle(fontFamily: 'Montserrat', fontSize: 20.0, color: Colors.black);
  var webView = Container();
  ProgressDialog pr;
  final GlobalKey<ScaffoldState> mScaffoldState =
  new GlobalKey<ScaffoldState>();

  FocusNode _textFocus = new FocusNode();
  @override
  void initState() {
    // TODO: implement initState
    StripeService.init();
    super.initState();
   getUserPreference();
  }

  @override
  Widget build(BuildContext context) {
    final textCard = Container(
      child:  Center(
          child: ShowUp(
            child: Text("Request For Seo",style: TextStyle(color:Colors.black,fontSize: 25,fontWeight: FontWeight.w700)),
            delay: delayAmount,
          )),
    );

    final titleCard = Card(
      elevation: 3,
      color: hexToColor("#f7fffb"),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      borderOnForeground: true,
      margin: EdgeInsets.only(right: 24, left: 24),
      child: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 6, left: 8, right: 6),
              child: TextField(
                textAlign: TextAlign.left,
                controller: nameController,
                decoration: new InputDecoration(
                    icon: Icon(
                      Icons.title,
                      size: 40,
                      color: hexToColor("#00a859"),
                    ),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding:
                    EdgeInsets.only(right: 8, bottom: 4, top: 10),
                    hintText: "Website Name"),
              ))
        ],
      ),
    );
    final deadlineCard = Card(
      elevation: 3,
      color: hexToColor("#f7fffb"),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      borderOnForeground: true,
      margin: EdgeInsets.only(right: 24, left: 24),
      child: Row(

        children: [
          SizedBox(width: 8,),
          Icon(
            Icons.calendar_today,
            size: 40,
            color: hexToColor("#00a859"),
          ),
          Container(
            height: 50,
            margin: EdgeInsets.only(top: 6, left: 8),
            child: InkWell(
              onTap: () async {
                DatePicker.showDatePicker(this.context,
                    theme: DatePickerTheme(
                      containerHeight: 210.0,
                    ),
                    showTitleActions: true,
                    minTime: DateTime(1950, 1, 1),
                    maxTime: DateTime(2050, 12, 31), onConfirm: (date) {
                      deadlineTime = '${date.year}-${date.month}-${date.day}';
                      setState(() {});
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
              },
              child: Center(
                  child: Text(
                    "    $deadlineTime",
                    style: TextStyle(color: Colors.black54, fontSize: 17),
                  )),
            ),
          )
        ],
      ),
    );
    final urlCard = Card(
      elevation: 3,
      color: hexToColor("#f7fffb"),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      borderOnForeground: true,
      margin: EdgeInsets.only(right: 24, left: 24),
      child: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 6, left: 8, right: 6),
              child: TextField(
                textAlign: TextAlign.left,
                controller: urlController,
                decoration: new InputDecoration(
                    icon: Icon(
                      Icons.web,
                      size: 40,
                      color: hexToColor("#00a859"),
                    ),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding:
                    EdgeInsets.only(right: 8, bottom: 4, top: 10),
                    hintText: "Website Url"),
              ))
        ],
      ),
    );
    final userNameCard = Card(
      elevation: 3,
      color: hexToColor("#f7fffb"),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      borderOnForeground: true,
      margin: EdgeInsets.only(right: 24, left: 24),
      child: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 6, left: 8, right: 6),
              child: TextField(
                textAlign: TextAlign.left,
                controller: userNameController,
                decoration: new InputDecoration(
                    icon: Icon(
                      Icons.account_circle_rounded,
                      size: 40,
                      color: hexToColor("#00a859"),
                    ),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding:
                    EdgeInsets.only(right: 8, bottom: 4, top: 10),
                    hintText: "Website Username"),
              ))
        ],
      ),
    );
    final passwordCard = Card(
      elevation: 3,
      color: hexToColor("#f7fffb"),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      borderOnForeground: true,
      margin: EdgeInsets.only(right: 24, left: 24),
      child: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 6, left: 8, right: 6),
              child: TextField(
                textAlign: TextAlign.left,
                controller: passwordController,
                decoration: new InputDecoration(
                    icon: Icon(
                      Icons.lock,
                      size: 40,
                      color: hexToColor("#00a859"),
                    ),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding:
                    EdgeInsets.only(right: 8, bottom: 4, top: 10),
                    hintText: "Website password"),
              ))
        ],
      ),
    );
    final keywordCard = Card(
      elevation: 3,
      color: hexToColor("#f7fffb"),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      borderOnForeground: true,
      margin: EdgeInsets.only(right: 24, left: 24),
      child: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 6, left: 8, right: 6),
              child: TextField(
                textAlign: TextAlign.left,
                controller: keywordController,
                decoration: new InputDecoration(
                    icon: Icon(
                      Icons.work,
                      size: 40,
                      color: hexToColor("#00a859"),
                    ),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding:
                    EdgeInsets.only(right: 8, bottom: 4, top: 10),
                    hintText: "Keywords"),
              ))
        ],
      ),
    );
    final urlPageCard = Card(
      elevation: 3,
      color: hexToColor("#f7fffb"),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      borderOnForeground: true,
      margin: EdgeInsets.only(right: 24, left: 24),
      child: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 6, left: 8, right: 6),
              child: TextField(
                textAlign: TextAlign.left,
                controller: pageUrlController,
                decoration: new InputDecoration(
                    icon: Icon(
                      Icons.web,
                      size: 40,
                      color: hexToColor("#00a859"),
                    ),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding:
                    EdgeInsets.only(right: 8, bottom: 4, top: 10),
                    hintText: "Page Url"),
              ))
        ],
      ),
    );
    final commentCard = Card(
      elevation: 3,
      color: hexToColor("#f7fffb"),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      borderOnForeground: true,
      margin: EdgeInsets.only(right: 24, left: 24),
      child: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 6, left: 8, right: 6),
              child: TextField(
                keyboardType: TextInputType.multiline,
                textAlign: TextAlign.left,
                controller: commentController,
                maxLines: 10,
                decoration: new InputDecoration(
                    icon: Icon(
                      Icons.description,
                      size: 40,
                      color: hexToColor("#00a859"),
                    ),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding:
                    EdgeInsets.only(right: 8, bottom: 4, top: 10),
                    hintText: "Description"),
              ))
        ],
      ),
    );
    final hostUserNameCard = Card(
      elevation: 3,
      color: hexToColor("#f7fffb"),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      borderOnForeground: true,
      margin: EdgeInsets.only(right: 24, left: 24),
      child: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 6, left: 8, right: 6),
              child: TextField(
                textAlign: TextAlign.left,
                controller: hostNameController,
                decoration: new InputDecoration(
                    icon: Icon(
                      Icons.account_circle_rounded,
                      size: 40,
                      color: hexToColor("#00a859"),
                    ),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding:
                    EdgeInsets.only(right: 8, bottom: 4, top: 10),
                    hintText: "Hosting Username"),
              ))
        ],
      ),
    );
    final hostPasswordCard = Card(
      elevation: 3,
      color: hexToColor("#f7fffb"),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      borderOnForeground: true,
      margin: EdgeInsets.only(right: 24, left: 24),
      child: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 6, left: 8, right: 6),
              child: TextField(
                textAlign: TextAlign.left,
                controller: hostPasswordController,
                decoration: new InputDecoration(
                    icon: Icon(
                      Icons.lock,
                      size: 40,
                      color: hexToColor("#00a859"),
                    ),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding:
                    EdgeInsets.only(right: 8, bottom: 4, top: 10),
                    hintText: "Hosting password"),
              ))
        ],
      ),
    );
    return Scaffold(
      key: mScaffoldState,
      backgroundColor: hexToColor("#f7fffb"),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            textCard,
            SizedBox(
              height: 30,
            ),
            buildType(),
            SizedBox(
              height: 10,
            ),
            titleCard,
            SizedBox(
              height: 10,
            ),
            urlCard,
            SizedBox(
              height: 10,
            ),
            userNameCard,
            SizedBox(
              height: 10,
            ),
            passwordCard,
            SizedBox(
              height: 10,
            ),
            keywordCard,
            selectedType == "Off page seo"?
            SizedBox(height: 10,):Container(),
            selectedType == "Off page seo"?
                urlPageCard:Container(height: 0,width: 0,),
            selectedType == "Technical seo"?
            SizedBox(
              height: 10,
            ):Container(),
            selectedType == "Technical seo"?
                hostUserNameCard:Container(height: 0,width: 0,),
            SizedBox(
              height: 10,
            ),
            selectedType == "Technical seo"?
            hostPasswordCard:Container(height: 0,width: 0,),
             SizedBox(height: 10),
            deadlineCard,
            SizedBox(
              height: 10,
            ),
            commentCard,

            SizedBox(
              height: 50,
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              child: GestureDetector(
                onTap: () {
                  if(selectedType == null){
                    Sbar().showSnackBar('Select SEO type', mScaffoldState);
                  }
              else if(nameController.text.isEmpty){
                 Sbar().showSnackBar('Enter your website name', mScaffoldState);
               }
              else if(urlController.text.isEmpty){
                 Sbar().showSnackBar('Enter your website url', mScaffoldState);
               }
               else if(userNameController.text.isEmpty){
                 Sbar().showSnackBar('Enter your website login username', mScaffoldState);
               }
               else if(passwordController.text.isEmpty){
                 Sbar().showSnackBar('Enter your website login password', mScaffoldState);
               }
               else if(keywordController.text.isEmpty){
                 Sbar().showSnackBar('Enter keywords', mScaffoldState);
               }
                  else if(selectedType == "Off page seo" && hostNameController.text.isEmpty){
                    Sbar().showSnackBar('Enter hosting username', mScaffoldState);
                  }
                  else if(selectedType == "Off page seo" && hostPasswordController.text.isEmpty){
                    Sbar().showSnackBar('Enter hosting password', mScaffoldState);
                  }
               else {
                 payViaNewCard(context);
               }
                },
                child: Container(
                  margin: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                  width: MediaQuery.of(context).size.width,

                  // color: hexToColor("#00a859"),
                  height: 50,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: hexToColor("#00a859"),
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                  ),
                  child: _isLoading
                      ? CircularProgress()
                      : Row(
                    children:[
                      SizedBox(width: 6,),
                      Icon(Icons.credit_card,color: Colors.white,size: 25,),
                      SizedBox(width: 20,),
                      Text(
                    "PAY 1000Rs. FOR SEO REQUEST",
                    textAlign: TextAlign.center,
                    style: style.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold,fontSize: 16),
                  ),
                  ])
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
          ],
        ),
      ),
    );
  }
  Widget buildType() {
    return Container(
        margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3.0),
          border: Border.all(width: 2, color: Colors.black26),
        ),
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: DropdownButtonHideUnderline(
            child: DropdownButton(
              items: category
                  .map((value) => DropdownMenuItem(
                child: Text(
                  value,
                  style: TextStyle(color: Colors.black),
                ),
                value: value,
              ))
                  .toList(),
              onChanged: (selected) {
                setState(() {
                  selectedType = selected;


                });
              },
              value: selectedType,
              isExpanded: true,
              hint: Text(
                ('SEO Type'),
                style: TextStyle(color: Colors.black),
              ),
            )));
  }
  _addSeoData() {

      HttpControllers()
          .requestSeo(
          name: nameController.text,
          websitrUrl: urlController.text,
          userName: userNameController.text,
          password: passwordController.text,
          keywords: keywordController.text,
         deadLine: deadlineTime,
         comment: commentController.text,
         id: userId,
        type: selectedType,
        pageUrl: pageUrlController.text,
        hostName: hostNameController.text,
        hostPassword: hostPasswordController.text,
      )
          .then((response) {
        setState(() {
          _isLoading = false;
          _registerBtnSize = 315;
          _btnBorder = 4;
          print("all $response");
        });
        var message;
        var status;
        String msg = 'Unpredicted Error';
       for(int i = 0; i<response.length; i++) {
         status = response[i]["response"];
         message = response[i]["msg"];
       }
        if (status == '200') {

          nameController.text = '';
          urlController.text = '';
          userNameController.text = '';
          passwordController.text = '';
          keywordController.text = '';
          commentController.text = '';
          deadlineTime = "Select Deadline";
          pageUrlController.text = "";
          hostNameController.text = "";
          hostPasswordController.text = "";
          msg = "Your request for Seo has been transferred to our expert.";
        } else if (response == '22') {
          // Database Error
          msg = 'Server is not responding!';
        } else if (response == '33') {
          // Unpredicted Error
          msg = 'Unpredicted Error';
        } else {
          // Random Error
          msg = 'Unpredicted Error';
        }
        Sbar().showSnackBar(msg, mScaffoldState);
      });

  }
  _sendNotificationTap() {

    HttpControllers()
        .sendNotification(
        user: userId,
        title: "Request For Seo",
        body: "$fName request for his ${nameController.text} website"
    )
        .then((response) {

      var message;
      var status;
      String msg = 'Unpredicted Error';
      for (int i = 0; i < response.length; i++) {
        status = response[i]["response"];
        message = response[i]["msg"];
      }

    });

  }
  Future getUserPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userId = prefs.getString("UserId");
    fName = prefs.getString("firstName");
   print("user id $userId");
  }

  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  payViaNewCard(BuildContext context) async {
    ProgressDialog dialog = new ProgressDialog(context);
    dialog.style(
        message: 'Please wait...'
    );
    await dialog.show();
    var response = await StripeService.payWithNewCard(
        amount: '10',
        currency: 'PKR'
    );


    await dialog.hide();
    if(response.message == "Transaction successful"){
      print("helloooo");
      _addSeoData();
      _sendNotificationTap();
    }
    Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text(response.message),
          duration: new Duration(milliseconds: response.success == true ? 1700 : 3000),
        )

    );


  }
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(width: 1.0, color: Colors.grey),
    borderRadius:
    BorderRadius.all(Radius.circular(5.0) //         <--- border radius here
    ),
  );
}
