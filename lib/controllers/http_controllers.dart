import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HttpControllers {
  //* * * * * * *   LOGIN   * * * * * * *
  Future login({String email, String password}) async {
    try {
      final json = jsonEncode({
        "email": "$email",
        "password": "$password",
      });
      var url = Uri.parse('http://139.59.80.110:8189/seo/auth');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }

  //* * * * * * *   Registration * * * * * * *//
  Future registerUser({
    String firstName,
    String lastName,
    String email,
    String location,
    String userName,
    String password,
  }) async {
    try {
      print("data $firstName");
      final json = jsonEncode({
        "first_name": "$firstName",
        "last_name": "$lastName",
        "address": "$location",
        "email": "$email",
        "password": "$password"
      });
      var url = Uri.parse('http://139.59.80.110:8189/seo/register');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});

      print("staty ${response.statusCode}");
      debugPrint("Response: " + response.body);
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];

        return response.body;
      } else if (response.statusCode == 409) {
        return "409";
      } else {
        // 33 is the ERR CODE
        return '22';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '22';
    }
  }

  //* * * * * * *  Update Password  * * * * * * *//
  Future updatePassword(
      {String oldPassword, String newPassword, String id}) async {
    try {
      final json = jsonEncode({
        "user_id": "$id",
        "old_password": "$oldPassword",
        "new_password": "$newPassword",
      });
      var url = Uri.parse('http://139.59.80.110:8189/seo/password/change');
      var response = await http
          .put(url, body: json, headers: {"Content-Type": "application/json"});

      print(response.statusCode);
      debugPrint("Response: " + response.body);
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else if (response.statusCode == 409) {
        return "409";
      } else {
        // 33 is the ERR CODE
        return '22';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '22';
    }
  }

  //* * * * * * *   UpdateProfile   * * * * * * *
  Future updateProfile(
      {String fName,
      String lName,
      String email,
      String address,
      String id}) async {
    try {
      final json = jsonEncode({
        "user_id": "$id",
        "first_name": "$fName",
        "last_name": "$lName",
        "email": "$email",
        "address": "$address"
      });
      var url = Uri.parse('http://139.59.80.110:8189/seo/user/update');
      var response = await http
          .put(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }

  //* * * * * * *   Get Tips   * * * * * * *
  Future getTips({String title, String description}) async {
    try {
      var url = Uri.parse('http://139.59.80.110:8187/seo/tips');
      var response =
          await http.get(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("data ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }

  //* * * * * * *  Get Profile   * * * * * * *
  Future getProfile({String id}) async {
    try {
      var url = Uri.parse('http://139.59.80.110:8189/seo/profile?user_id=$id');
      var response =
          await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }

  //
  //* * * * * * *   Get Page Rank   * * * * * * *
  Future getRank({String domain}) async {
    try {
      var url = Uri.parse(
          'https://openpagerank.com/api/v1.0/getPageRank?domains[]=$domain');
      var response = await http.get(url,
          headers: {"API-OPR": "g8o48gkkkw8kw4cogssg44ksokg8kwwgscksow4k"});
      debugPrint("Response: " + response.body);
      print("data ${response.body}");
      Map<String, dynamic> map = jsonDecode(response.body);
      var result = map["response"];
      print("map $result");
      if (response.statusCode == 200) {
        return result;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Add Discussion   * * * * * * *
  Future addDiscussion({String name, String message}) async {
    try {
      final json = jsonEncode({
        "name": "$name",
        "message": "$message",
      });
      var url = Uri.parse('http://139.59.80.110:8188/seo/discuss');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Get Discussion   * * * * * * *
  Future getDiscussion() async {
    try {
      var url = Uri.parse('http://139.59.80.110:8188/seo/discussions');
      var response =
      await http.get(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("data ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Send Message   * * * * * * *
  Future sendMessage({String name,String id, String message}) async {
    try {
      final json = jsonEncode({
        "name": "$name",
        "user" : id,
        "message": "$message",
      });
      var url = Uri.parse('http://139.59.80.110:8188/seo/message');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Get Messages   * * * * * * *
  Future getMessages(int id) async {
    try {
      var url = Uri.parse('http://139.59.80.110:8188/seo/get/reply?reply_to=$id');
      var response =
      await http.get(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("data ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Get User Messages   * * * * * * *
  Future getUserMessage(int id) async {
    try {
      var url = Uri.parse('http://139.59.80.110:8188/seo/user/message?user=$id');
      var response =
      await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("data ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Get Messages   * * * * * * *
  Future getAdminMessages(int id) async {
    try {
      var url = Uri.parse('http://139.59.80.110:8188/seo/get/reply?reply_to=$id');
      var response =
      await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("data ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Request Seo   * * * * * * *
  Future requestSeo({String name,String websitrUrl,String userName,String password,
    String keywords,String deadLine,String comment,String id,String type,String pageUrl,
    String hostName, String hostPassword}) async {
    try {
      final json = jsonEncode({
        "website_name": "$name",
        "website_url" : "$websitrUrl",
        "login_username":"$userName",
        "login_password":"$password",
        "keywords":"$keywords",
        "deadline":"$deadLine",
        "additional_comment":"$comment",
        "user":"$id",
        "type": "$type",
        "url": "$pageUrl",
        "host_name": "$hostName",
        "host_password": "$hostPassword"
      });
      var url = Uri.parse('http://139.59.80.110:8186/seo/service');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Get Progress   * * * * * * *
  Future getSeoProgress(int id) async {
    try {
      var url = Uri.parse('http://139.59.80.110:8186/seo/show/progress?user=$id');
      var response =
      await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Send Notification  * * * * * * *
  Future sendNotification({String user, String title, String body}) async {

    try {
      final json = jsonEncode({
        "user": "$user",
        "title" : "$title",
        "body": "$body",
        "status":false
      });
      var url = Uri.parse('http://139.59.80.110:8186/seo/user/notify');
      var response = await http
          .post(url, body: json, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("res ${response.body}");
      if (response.statusCode == 200) {
        return [
          {"response": "200", "msg": "${response.body}"}
        ];
      } else {
        return '55';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Get Notification   * * * * * * *
  Future getNotification(int id) async {
    try {
      var url = Uri.parse('http://139.59.80.110:8186/seo/admin/view/notification?user=$id');
      var response =
      await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("usermessage ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Update Notification   * * * * * * *
  Future updateNotification(int notifyId) async {
    print("notifyId $notifyId");
    try {
      var url = Uri.parse('http://139.59.80.110:8186/seo/admin/seen?user=$notifyId');
      var response =
      await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("usermessage ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   All Seo Done   * * * * * * *
  Future getAllSeoDone(int id) async {
    try {
      var url = Uri.parse('http://139.59.80.110:8186/seo/done/seo?user=$id');
      var response =
      await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("usermessage ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
  //* * * * * * *   Get User SEO Request   * * * * * * *
  Future getSeoRequest(int id) async {
    try {
      var url = Uri.parse('http://139.59.80.110:8186/seo/view/request?user=$id');
      var response =
      await http.post(url, headers: {"Content-Type": "application/json"});
      debugPrint("Response: " + response.body);
      print("data ${response.body}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        // 33 is the ERR CODE
        return 'zero';
      }
    } on Exception catch (e) {
      debugPrint("Exception Catched: ${e.toString()}");
      // 55 is the ERR CODE
      return '55';
    }
  }
}
